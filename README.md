# Django Tutorial Poll App

Для начала необходимо установить пакеты и активировать виртуальную среду: 

```bash
$ pipenv install
$ pipenv shell
```

После, нужно создать таблицы в БД: 

```bash
$ python manage.py makemigrations
$ python mananage.py migrate
```

Создать администратора:

```Bash
$ python manage.py createsuperuser
```

Запустить dev сервер:

```bash
$ python manage.py runserver
```

Веб-приложение будет доступно через бразуер по адресу http://127.0.0.1:8000, а админ-панель http://127.0.0.1:8000/admin.