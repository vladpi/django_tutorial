from django.contrib import admin

from .models import Question, Choice

class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('question_text', 'publication_date', 'was_published_recently')
    inlines = [ChoiceInline]
    list_filter = ['publication_date']
    search_fields = ['question_text']

    fieldsets = [
        (None, {'fields': ['question_text']}),
        ('Информация о дате', {'fields': ['publication_date'], 'classes': ['collapse']}),
    ]
    

admin.site.register(Question, QuestionAdmin)
# admin.site.register(Choice)
