import datetime

from django.db import models
from django.utils import timezone


class Question(models.Model):
    question_text = models.CharField('Вопрос', max_length=200)
    publication_date = models.DateTimeField('Дата публикации', default=timezone.now)
    
    class Meta:
        verbose_name = 'вопрос'
        verbose_name_plural = 'вопросы'

    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.publication_date <= now
        
    was_published_recently.admin_order_field = 'publication_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Опубликовано недавно?'


class Choice(models.Model):
    choice_text = models.CharField('Текст ответа', max_length=200)
    votes = models.IntegerField('Голоса', default=0)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'ответ'
        verbose_name_plural = 'ответы'

    def __str__(self):
        return self.choice_text

